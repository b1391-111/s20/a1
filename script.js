/*
	Activity:

	Create an array of objects called courses:

		Each object should have the following fields:

		id -string
		name - string
		description - string
		price - number
		isActive - boolean

	The array should have a minimum of 4 objects.

	//Create
		-Create an arrow function called addCourse which allows us to add a new object into the array. This function should receive the following the data:
			-id,
			-name,
			-description,
			-price
			-isActive
		-This function should be able to show an alert after adding into the array:
			"You have created <nameOfCourse>. Its price is <priceOfCourse>"
		-push method (to push an object in the array of objects)

	//Retrieve/Read

		-Create an arrow function getSingleCourse which allows us to find a particular course by the providing the course's id.

			-Show the details of the found course in the console.
			-return the found course
			- find method

		-Create an arrow function getAllCourses which is able to show all of the items/objects in the array in our console.

			-return the courses array
			- any method (so long as you can return all the courses)

	//Update

		-Create an arrow function called archiveCourse which is able to update/re-assign the isActive property of a particular course, update the isActive property to false. This function should be able to receive the particular index number of the item as an argument.

			-show the updated course in the console.
			- index & dot notation and assignment operator

	
	//Delete
		-Create an arrow function called deleteCourse which is able to delete the last course object in the array.
		-pop method


	Stretch Goals (Added challenge only/No need to accomplish both):

	Create an arrow function which can show all of the active courses only. Show the active courses in the console. See .filter()
	- you may store the result of filter method in a variable

	In the archiveCourse function, the function should instead receive the name of the course and use the name to find the index number of the course.  See findIndex()
	- - you may store the result of filter method in a variable

*/

let courses = [
	{
		id: "1",
		name: "Python 101",
		description: "Learn the basics of python.",
		price: 15000,
		isActive: true

	},
	{
		id: "2",
		name: "CSS 101",
		description: "Learn the basics of CSS.",
		price: 10500,
		isActive: true

	},
	{
		id: "3",
		name: "CSS 102",
		description: "Learn an advanced CSS.",
		price: 15000,
		isActive: false

	},
	{
		id: "4",
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its Laravel framework.",
		price: 20000,
		isActive: true
	}
];

// CREATE
const addCourse = (id, name, description, price, isActive) => {
    let newCourse = {
        id,
        name,
        description,
        price,
        isActive
    }
    courses.push(newCourse);
    alert(`You have created ${name}. Its price is ${price}"`);
}
addCourse("5", "CRUD 101", "Learn the basic operations of CRUD", 25000, false);
addCourse("6", "CRUD 102", "Learn the operations of CRUD", 35000, false);
console.log(`CREATE: addCourse("5", "CRUD 101", "Learn the basic operations of CRUD", 25000, true): `, courses);


// READ
const getSingleCourse = (course_id) => {
	let result = courses.filter(course => course.id === course_id);
	console.log(`READ: getSingleCourse("2"): `, result);
} 
getSingleCourse("2");


//UPDATE
const archiveCourse = (course_name) => {
	let course_index = courses.findIndex(course => course.name === course_name);
	courses[course_index].isActive = !courses[course_index].isActive;
	console.log(`UPDATE: archiveCourse("CRUD 101"): `, courses);
}
archiveCourse("CRUD 101");


//DELETE
const deleteCourse = () => {
	courses.pop();
}
deleteCourse();
console.log(`DELETE: deleteCourse(): `, courses);


// Stretch Goals (Added challenge only/No need to accomplish both):

// 	Create an arrow function which can show all of the active courses only. Show the active courses in the console. See .filter()
// 	- you may store the result of filter method in a variable

// 	In the archiveCourse function, the function should instead receive the name of the course and use the name to find the index number of the course.  See findIndex()
// 	- - you may store the result of filter method in a variable

// STRETCH GOALS
const activeCourses = courses.filter(course => course.isActive);

console.log(`Active Courses: `, activeCourses);